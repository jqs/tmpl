<?php

namespace jqs\tmpl;

define('TMPL_VERSION', '2.5');
define('LD_DISP', 0);
define('LD_RET', 1);
define('LD_ARR', 10);
define('TK_DISP', 0);
define('TK_LD_PARSE', 1);
define('TK_LD_DISP', 2);
define('TK_VAR', 3);
define('TK_PARSE', 5);
define('TK_PARSE_ARR', 6);

class tmpl {
	const LD_DISP = 0;
	const LD_RET = 1;
	const LD_ARR = 10;
	const TK_DISP = 0;
	const TK_LD_PARSE = 1;
	const TK_LD_DISP = 2;
	const TK_VAR = 3;
	const TK_PARSE = 5;
	const TK_PARSE_ARR = 6;
	protected $templateFileName	= null;
	protected $tokens		= array();
	protected $return		= false;
	protected $tagOpen		= '[%';
	protected $tagClose		= '%]';
	protected $length		= 100;
	protected $counter		= 0;
	protected $data			= array();

	public function __construct() {
		$this->init();
	}

	public function TemplateFile($filespec) {
		if (is_string($filespec) && strlen($filespec)) {
			$this->templateFileName = $filespec;
		}
	}

	public function openTag($tag) {
		if (is_string($tag) && strlen($tag)) {
			$this->tagOpen = $tag;
		}
	}

	public function closeTag($tag) {
		if (is_string($tag) && strlen($tag)) {
			$this->tagClose = $tag;
		}
	}

	public function reset() {
		$this->init();
	}

	public function addToken($token, $type, $data) {
		if (is_string($token) && strlen($token)) {
			$this->tokens[$token] = array($type, $data);
		}
	}

	public function addTokens($tokens) {
		if (is_array($tokens)) {
			// no data testing here!
			$this->tokens = $this->tokens+(array) $tokens;
		}
	}

	public function delToken($token) {
		if (is_string($token) && strlen($token) && isset($this->tokens[$token])) {
			unset($this->tokens[$token]);
		}
	}

	public function removeAllTokens() {
		unset($this->tokens);
		$this->tokens = array();
	}

	public function parseTemplateFile() {
		if (strlen($this->templateFileName)) {
			$this->return = false;
			$this->parseFile($this->templateFileName);
		}
	}

	public function parseTemplateString($template) {
		if (is_string($template) && strlen($template)) {
			$this->return = false;
			$this->parseString($template);
		}
	}

	public function getParsedTemplateFile() {
		if (strlen($this->templateFileName)) {
			$this->stringReset();
			$this->return = true;
			return $this->parseFile($this->templateFileName);
		}
	}

	public function getParsedTemplateString($template) {
		if (is_string($template) && strlen($template)) {
			$this->stringReset();
			$this->return = true;
			return $this->parseString($template);
		}
	}

	public function loadFile($filespec, $type, &$data) {
		$return = false;
		if (is_string($filespec) && strlen($filespec) && is_file($filespec)) {
			$file = file_get_contents($filespec);
			switch ($type) {
				case LD_DISP:
					$this->printLine($file);
					break;
				case LD_RET:
					$data = $file;
					break;
				case LD_ARR:
					$data = preg_split('/\n|\r|\n\r/', $file);
					break;
				default:
					return false;
			}
			$return = true;
		}
		return $return;
	}

	private function init() {
		$this->templateFileName = null;
		$this->tokens = array();
		$this->return = false;
		$this->tagOpen = '[%';
		$this->tagClose = '%]';
		$this->stringReset();
	}

	protected function parseFile($filespec) {
		if (is_file($filespec)) {
			if ($this->loadFile($filespec, LD_ARR, $data)) {
				$this->parseArray($data);
			} else {
				// todo err loading file
			}
		}
		if ($this->return) {
			return $this->getString();
		}
	}

	protected function parseString($data) {
		if (is_string($data)) {
			$this->parseArray(preg_split('/\n|\r|\n\r/', $data));
		}
		if ($this->return) {
			return $this->getString();
		}
	}

	protected function parseArray($array) {
		foreach ($array as $line) {
			$this->parseLine($line . "\n"); // todo allow for userdefined line endings
		}
	}

	protected function parseLine($line) {
		$posA = strpos($line, $this->tagOpen);
		if (!($posA === false)) {
			$lineA = substr($line, 0, $posA);
			$posB = strpos($line, $this->tagClose);
			if (!($posB === false)) {
				$lineB = substr($line, $posB+strlen($this->tagClose));
				$token = trim(substr($line, $posA+strlen($this->tagOpen), ($posB-$posA-strlen($this->tagOpen))));
				$this->printLine($lineA);
				$this->getToken($token);
				if (strlen($lineB)>0) {
					$this->parseLine($lineB); // Note: may be more than one token on a line...
				}
			} else {
				$this->printLine($line); // Note we don't support tags spanning mulitple lines
			}
		} else {
			$this->printLine($line);
		}
	}

	protected function getToken($token) {
		if(isset($this->tokens[$token])) {
			list($type, $data) = $this->tokens[$token];
			switch($type) {
				case TK_DISP:
					$this->printLine($data);
					break;
				case TK_LD_PARSE:
					if ($this->loadFile($data, LD_ARR, $fileData)) {
						$this->parseArray($fileData);
					} else {
						// todo unloaded file
					}
					break;
				case TK_LD_DISP:
					if ($this->loadFile($data, LD_RET, $fileData)) {
						$this->printLine($fileData);
					} else {
						// todo unloaded file
					}
					break;
				case TK_PARSE:
					$this->parseLine($data);
					break;
				case TK_PARSE_ARR:
					$this->parseArray($data);
					break;
				default:
					break; // todo undefined token type?
			}
		} else {
			// todo unfound token
		}
	}

	protected function printLine($line) {
		if (strlen($line)) {
			if ($this->return) {
				$this->addToString($line);
			} else {
				echo $line;
			}
		}
	}

	protected function stringReset() {
		unset($this->data);
		$this->length = 100;
		$this->counter = 0;
		$this->data = array();
	}

	protected function getString() {
		return implode("", $this->data);
	}

	protected function addToString($line) {
		if ($this->counter >= $this->length) {
			$tempData = $this->getString();
			$this->stringReset();
			$line = $tempData . $line;
		}
		$this->data[$this->counter] = $line;
		$this->counter++;
	}

}                 