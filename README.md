tmpl [![Gitter chat](https://badges.gitter.im/jqs/tmpl.png)](https://gitter.im/jqs/tmpl)
====

A fast parsing library for template files. Designed to seperate content (html) from code (php)


todo
====

php
- add better robust error handling
- add installation notes / PSR4
- add to compopser

python
- create first instance in python